# Inleiding

Iedere lesmodule is gestuurd rond een bepaald thema en neemt gemiddeld 2 weken in beslag.

Er zijn twee websites waar echt alles terug te vinden over Beckhoff.

* https://www.beckhoff.com - documentatie over de modules en software (physical systems)
* https://infosys.beckhoff.com -  de verschillende libraries en examples

Je moet soms wel weten waar te zoeken. De zoekfunctie is erbarmelijk. Download alvast de software TwinCAT3 (niet de 2) onder de sectie Automation.
Neem de Engineering version XAE en niet enkel de Runtime XAR. De information System kan je ook downloaden, wat de offline versie van infosys.beckhoff is.
Op de online infosys vind je het belangrijkste terug onder TwinCAT3, dan TE1000 XAE en vervolgens PLC.

HINT: http://www.contactandcoil.com is een website die de basis tot in detail zeer goed uitlegt. Het is soms langdradig maar geeft veel details mee.

# Introductie

Een introductie tot TwinCAT terug te vinden op de website contact and coils en behandeld

* Wat is Beckhoff
* Wat is TwinCAT3

# [Powerpoints](Powerpoints)

# Starten met TwinCAT

De basis wordt in alle drie documenten behandeld.

* Contact and Coil is to the point
* Starters Guide is een uitbreiding
* Terminal documentatie is een zeer gedetailleerde intro die bij alle terminals aanwezig zijn

# Reading material

Leesmateriaal dat je zal helpen om TC beter te begrijpen



